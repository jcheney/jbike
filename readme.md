# Cycling Every road in Logan.

## Motivation

- Improve [OSM traces](https://www.openstreetmap.org/#map=14/41.7387/-111.8292&layers=G), of which there were essentially none in Logan
- Improve my bike maintenance and repair skills
- Improve my own fitness
- Improve my carbon footprint
- Fun

## Data Handling

- Visualize using [derive](https://erik.github.io/derive/)
- Combine each month using [viking](https://github.com/viking-gps/viking) since the above has a bug where it doesn't handle large numbers of files very well
- Animated GIF using [rainbow-roads](https://github.com/NathanBaulch/rainbow-roads)

## Navigation

- Using [osmand](https://osmand.net/)
    - Custom rendering style to hide driveways, deemphasize parking aisles, etc. [documentation](http://docs.osmand.net/en/main@latest/development/osmand-file-formats/osmand-rendering-style)
- Using a used Cat S41 smartphone for durability, low cost, and huge battery

---

![progress_2022-03-02.gif]()
